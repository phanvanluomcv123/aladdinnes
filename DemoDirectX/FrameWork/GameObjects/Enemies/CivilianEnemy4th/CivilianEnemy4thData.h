#pragma once
//pre define
class CivilianEnemy4thState;
class CivilianEnemy4th;

class CivilianEnemy4thData
{
public:
	CivilianEnemy4thData();
	~CivilianEnemy4thData();

	CivilianEnemy4th     *civilianEnemy4th;
	CivilianEnemy4thState *state;

protected:

};